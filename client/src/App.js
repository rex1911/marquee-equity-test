import Search from "./screens/Search/Search";
import List from "./screens/List/List";
import { BrowserRouter, Route, Switch } from "react-router-dom";

const App = () => {
	return (
		<BrowserRouter>
			<Switch>
				<Route path="/list">
					<List />
				</Route>
				<Route path="/">
					<Search />
				</Route>
			</Switch>
		</BrowserRouter>
	);
};

export default App;
