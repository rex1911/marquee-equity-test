import { useState } from "react";
import axios from "axios";
import SelectSearch from "react-select-search";
import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";

import "./Search.css";

const Search = () => {
	const [selected, changeSelected] = useState({});

	const history = useHistory();

	const handleChange = (val) => {
		let cin = val.split("/")[0];
		let name = val.split("/")[1];
		changeSelected({ cin, name });
	};

	const fetchFromAPI = (query) => {
		if (query === "") {
			return;
		}

		let body = { query };

		return new Promise(async (resolve, reject) => {
			const res = await axios({
				url: "http://localhost:5000/api/search",
				method: "POST",
				data: body,
			});

			resolve(res.data);
		});
	};

	const handleClick = async () => {
		if (Object.keys(selected).length === 0) {
			console.log("Empty");
			return;
		}

		try {
			await axios({
				url: "http://localhost:5000/api/companies",
				method: "POST",
				data: selected,
			});
			history.push("/list");
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<>
			<div className="mainDiv">
				<h1>Search for companies</h1>
				<div className="searchDiv">
					<SelectSearch
						options={[]}
						getOptions={fetchFromAPI}
						debounce={1000}
						onChange={handleChange}
						search
					/>
					<Button variant="contained" color="primary" onClick={handleClick}>
						Save
					</Button>
				</div>
			</div>
		</>
	);
};

export default Search;
