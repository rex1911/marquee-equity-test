import { useEffect, useState } from "react";
import axios from "axios";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { useHistory } from "react-router-dom";

import "./List.css";
import { Button } from "@material-ui/core";

const List = () => {
	const [tableData, setTableData] = useState(["1"]);

	const history = useHistory();

	useEffect(() => {
		const fetchData = async () => {
			let res = await axios.get("http://localhost:5000/api/companies");
			setTableData(res.data);
		};
		fetchData();
	}, []);

	const handleBtnClick = () => {
		history.push("/");
	};

	return (
		<div className="mainDivList">
			<div className="secondaryDivList">
				<Button variant="contained" color="primary" fullWidth onClick={handleBtnClick}>
					Add Company
				</Button>
				<div className="listTable">
					<TableContainer component={Paper}>
						<Table>
							<TableHead>
								<TableRow>
									<TableCell>CIN</TableCell>
									<TableCell>Company Name</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{tableData.map((data) => (
									<TableRow key={data.cin}>
										<TableCell>{data.cin}</TableCell>
										<TableCell>{data.name}</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</TableContainer>
				</div>
			</div>
		</div>
	);
};

export default List;
