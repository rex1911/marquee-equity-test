const FormData = require("form-data");
const axios = require("axios");
const { parse } = require("node-html-parser");
const Company = require("../models/model");

module.exports.search = async (req, res) => {
	const { query } = req.body;
	const body = new FormData();

	body.append("search", query);
	body.append("filter", "company");

	let bodyHeaders = body.getHeaders();

	const searchRes = await axios({
		url: "https://www.zaubacorp.com/custom-search",
		method: "POST",
		headers: {
			...bodyHeaders,
		},
		data: body,
	});
	let html = parse(searchRes.data);
	let elements = html.querySelectorAll("div");

	let toSend = [];
	elements.forEach((element) => {
		let fullID = element.id;
		let extractedID = fullID.split("/")[2] + "/" + element.text;
		toSend.push({ value: extractedID, name: element.text });
	});
	res.json(toSend);
};

module.exports.addCompany = async (req, res) => {
	try {
		const { cin, name } = req.body;
		let data = { cin, name };
		const company = new Company(data);
		const saved = await company.save();
		res.status(200).json(saved);
	} catch (error) {
		const response = {
			error: true,
			errorMessage: "Something went wrong. Please try again.",
		};
		res.status(500).json(response);
	}
};

module.exports.getCompanies = async (req, res) => {
	let company = await Company.find({});
	res.status(200).json(company);
};
