const express = require("express");
const bodyParser = require("body-parser");
const routes = require("./routes/routes");
const mongoose = require("mongoose");

const app = express();

//= =======================
// MongoDB setup
//= =======================
mongoose.connect(process.env.MONGO_URI || "mongodb://localhost/mqe", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
});

//=======================
// Middleware
//=======================
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//======================
//     ROUTES
//======================
app.use("/api", routes);
app.get("/", (req, res) => {
	res.send("Serving API");
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
	// eslint-disable-next-line no-console
	console.log(`App listening on port ${PORT}`);
});
