const mongoose = require("mongoose");

const companySchema = new mongoose.Schema({
	cin: String,
	name: String,
});

module.exports = mongoose.model("Company", companySchema);
