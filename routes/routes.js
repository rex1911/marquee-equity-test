const { Router } = require("express");
const { search, addCompany, getCompanies } = require("../controllers/controllers");

const router = Router();

router.post("/search", search);
router.post("/companies", addCompany);
router.get("/companies", getCompanies);

module.exports = router;
